package com.sarawut.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.sarawut.app2.databinding.FragmentMinusBinding
import com.sarawut.app2.databinding.FragmentPlusBinding


class MinusFragment : Fragment() {

    private var _binding: FragmentMinusBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMinusBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar!!.title = "Minus"

        view.findViewById<ImageButton>(R.id.btn_result).setOnClickListener {
            val num1 = binding?.numm1?.text.toString().toInt()
            val num2 = binding?.numm1?.text.toString().toInt()

            if(num1 != null && num2 != null) {
                val result = num1 + num2
                val action =
                    MinusFragmentDirections.actionMinusFragmentToAnswerFragment(result = result)
                view.findNavController().navigate(action)
            }
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}